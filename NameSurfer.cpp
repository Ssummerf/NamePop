#include <iostream>
#include "NameSurfer.h"
#include <string>
#include <iomanip>

using namespace std;
NameSurfer:: NameSurfer(){
}

NameSurfer:: NameSurfer(string filename){
	data = NameSurferDataBase(filename);
}

void NameSurfer:: promptUser(){
	int Temp = 0;
	while(Temp != 3){
	std:: cout << endl << "1:Enter a name to be searched." << endl << "2:Enter a year" << endl << "3:Exit" << endl << "Enter a choice:";
    cin >> Temp;


    if(Temp == 1)
    	printAll();
    

    if(Temp == 2)
    	compare();
}

}

//Print out all decades for a given name.
void NameSurfer:: printAll(){

	string name = "Temp";
	std:: cout << "Enter in a name:";
	cin.ignore();
	getline(cin, name);
	cout << name;
	cout << endl;

	NameSurferEntry temp = this->data.findEntry(name);

	for(int i = 1900; i < 2020; i+=10){
		std::cout << i;
		for(int j = temp.getRank(i); j >= 50; j-=50){
			std::cout << "*";
		}
		std::cout << "-" << temp.getRank(i) << endl;
	}

	std:: cout << endl;

}

//Compare two or more names.
void NameSurfer:: compare(){

	int nameNum = 0;
	string names[10];
	string name;
	int decade = 1900;
	std:: cout << "Enter a year:" ;
	cin >> decade;

	std:: cout << "Enter how many Names you want to compare:" ;
	cin >> nameNum;

	for(int i = 1; i <= nameNum; i++){
		std:: cout << "Enter name " << i << ":";
		cin >> name;
		names[i] = name;
	}



	std:: cout << endl << "You selected the decade " << decade << endl;
	NameSurferEntry placeHolder;

	for(int i = 1; i <= nameNum; i++){
		NameSurferEntry temp = this->data.findEntry(names[i]);
		std::cout << setw(15) << left << temp.getName()  ;

		if(temp.getRank(decade) > 50){
		for(int j = temp.getRank(decade); j >= 50; j-=50){
			std::cout << "*";
		}
		}
		std::cout << "-" << temp.getRank(decade) << endl;
	}

}