#ifndef NAMESURFER
#define NAMESURFER

#include "NameSurferDataBase.cpp"
#include "NameSurferEntry.cpp"


using namespace std;

class NameSurfer {

    public:
     NameSurfer(string filename);
     NameSurfer();
     void promptUser();
     void compare();
     void printAll();


    private:
    NameSurferDataBase data;

};

#endif