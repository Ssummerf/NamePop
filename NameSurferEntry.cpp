
#include <iostream>
#include <string>
#include <vector>
#include <iomanip>
#include "NameSurferEntry.h"


using namespace std;

ostream& operator<<(ostream &output, NameSurferEntry &temp ){
	output << std::fixed << std::setprecision(2) << temp.getName() << endl;
    return output;
}

bool NameSurferEntry::operator<(NameSurferEntry& rhs){
	return getName() < rhs.getName();
}
bool NameSurferEntry::operator!=(NameSurferEntry& rhs){
	return getName() != rhs.getName();
}
bool NameSurferEntry::operator<=(NameSurferEntry& rhs){
	return getName() <= rhs.getName();
}
bool NameSurferEntry::operator>(NameSurferEntry& rhs){
	return getName() > rhs.getName();
}
bool NameSurferEntry::operator>=(NameSurferEntry& rhs){
	return getName() >= rhs.getName();
}
bool NameSurferEntry::operator==(NameSurferEntry& rhs){
	return getName() == rhs.getName();
}

NameSurferEntry::NameSurferEntry(){
	;
}
NameSurferEntry:: NameSurferEntry(string line){

string delim = " ";
//size_t pos = 0;
int counter = 0;
//string token;
year.resize(13);


size_t prev = 0, pos = 0;
do
{
    pos = line.find(delim, prev);
    if (pos == string::npos) pos = line.length();
    string token = line.substr(prev, pos-prev);
    prev = pos + delim.length();
    if(counter == 0)
    	this->name = token;
    else{
    	year[counter-1] = stoi(token);
    
    }
    counter++;
}
while (pos < line.length() && prev < line.length());


//Parse through the line of text with space as the delimiter. 
//Split it into the name / years to be put into the NSEntry

}


string NameSurferEntry::getName(){
	return this->name;
}

//Calculate the actual position in the vector from a given decade, then return it.
int NameSurferEntry::getRank(int decade){
	int actualpos = (decade - 1900) / 10;
	if(actualpos <=12)
		return year[actualpos];
	else
		return -1;
}

bool NameSurferEntry:: empty(){
	int checker = 0;
	for(int decade = 1900; decade < 2020; decade++){
		checker += getRank(decade);
	}
	if(checker == 0)
		return true;
	else
		return false;
}