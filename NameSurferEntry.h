#ifndef NAMESURFERENTRYH
#define NAMESURFERENTRYH
#include <vector>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

class NameSurferEntry {
            //Overload your Boolean operators
            //Do a Friend Overload for cout<<
public:
	friend ostream &operator<<( ostream &output, NameSurferEntry &temp );
	bool operator<(NameSurferEntry& rhs);
	bool operator!=(NameSurferEntry& rhs);
	bool operator<=(NameSurferEntry& rhs);
	bool operator>(NameSurferEntry& rhs);
	bool operator>=(NameSurferEntry& rhs);
	bool operator==(NameSurferEntry& rhs);

	NameSurferEntry();
	NameSurferEntry(string line);
	string getName();
	int  getRank(int  decade);
	bool empty();
private:
	vector<int> year;
	string name;
};

#endif
