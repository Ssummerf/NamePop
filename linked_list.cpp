#ifndef LIST_CPP
#define LIST_CPP
#include "linked_list.h"
#include <iostream>
#include <string>
using namespace std;

template <class NODETYPE>
linked_list<NODETYPE>::linked_list() {
        first = NULL;
        current = NULL;
        last = NULL;
}

template <class NODETYPE>
linked_list<NODETYPE>::~linked_list() {
        current = first;
        while (current != NULL) {
                first = first->next;
                delete current;
                current = first;
        }
        current = last = first = NULL;
}
template <class NODETYPE>
linked_list<NODETYPE>::linked_list(linked_list<NODETYPE> &originalList) {
        ListNode<NODETYPE> *tmpPtr = new ListNode<NODETYPE>;
        tmpPtr = originalList.first;

        while (tmpPtr != NULL)
        {
                ListNode<NODETYPE> *newNode = new ListNode<NODETYPE>;
                newNode->data = tmpPtr->data;
                if(tmpPtr == originalList.first){
                        first = newNode;
                        first->next = NULL;
                }else{
                        last->next = newNode;
                }
                last = newNode;
                tmpPtr = tmpPtr->next;
                if (tmpPtr->next == NULL)
                {
                        break;
                }
        }
}

template <class NODETYPE>
bool linked_list<NODETYPE>::Remove(NODETYPE & value) {
        if (!IsEmpty()) {
                ListNode<NODETYPE> *temp;
                // A variable used to point to the data that is going to be
                // deleted.

                if ((first->data == value) && (first == last)) {
                        temp = first;
                        value = temp->data;
                        first = last = current = NULL;
                        delete temp;
                        return true;
                }
                else if (first->data == value) {
                        temp = first;
                        value = temp->data;
                        first = first->next;
                        delete temp;
                        return true;
                }
                else {
                        current = first;
                        while ((current != last) && (current->next->data != value)) {
                                current = current->next;
                        }
                        if (current == last)
                                return false;
                        else if (current->next == last) {
                                temp = last;
                                value = temp->data;
                                last = current;
                                last->next = NULL;
                                delete temp;
                        }
                        else {
                                temp = current->next;
                                value = temp->data;
                                current->next = temp->next;
                                delete temp;
                        }
                        return true;
                }
        }
        else cout << "Linked List Remove Failed: The list is empty.\n";
        return false;
}

        template <class NODETYPE>
bool linked_list<NODETYPE>::InsertInOrder(NODETYPE value)
{
        if (IsEmpty()) {
                // A variable used to point to data that is being inserted.
                ListNode<NODETYPE> *temp = new ListNode<NODETYPE>;

                if (temp == NULL) return false;

                temp->data = value;

                first = last = current = temp;
                last->next = NULL;
        }
        else if (value >= last->data) {
                return InsertRear(value);
        }
        else if (first->data >= value) {
                return InsertFront(value);
        }
        else {
                // A variable used to point to data that is being inserted.
                ListNode<NODETYPE> *temp = new ListNode<NODETYPE>;

                if (temp == NULL) return false;

                temp->data = value;

                current = first;
                while (current->next->data < temp->data) {
                        current = current->next;
                }
                temp->next = current->next;
                current->next = temp;
                return true;
        }
    return false;
}
template <class NODETYPE>
bool linked_list<NODETYPE>::InsertFront(NODETYPE value) {
        // A variable used to point to data that is being inserted.
        ListNode<NODETYPE> *temp = new ListNode<NODETYPE>;

        if (temp == NULL) return false;

        temp->data = value;
        if (first == NULL)
                temp->next = NULL;
        else
                temp->next = first;
        first = current = temp;

        return true;
}
template <class NODETYPE>
bool linked_list<NODETYPE>::InsertRear(NODETYPE value) {
        // A variable used to point to data that is being inserted.
        ListNode<NODETYPE> *temp = new ListNode<NODETYPE>;

        if (temp == NULL) return false;
        temp->data = value;
        if (last == NULL) {
                last = temp;
                first = last;
        }
        else
                last->next = temp;
        last = current = temp;
        last->next = NULL;

        return true;
}

template <class NODETYPE>
bool linked_list<NODETYPE>::IsEmpty() const {
        return (first == NULL);
}

template <class NODETYPE>
bool linked_list<NODETYPE>::Search(NODETYPE value) {
        if (IsEmpty()) {
                cout << "List is Empty\n";
                return false;
        }
        else {
                current = first;
                while ((current != NULL) && (current->data != value)) {
                        current = current->next;
                }
                if (current == NULL) {
                        return false;
                }
                else {

                        return true;
                }
        }
}


template <class NODETYPE>
void linked_list<NODETYPE>::PrintAll() {
        if (!IsEmpty()) {
                current = first;
                while (current != NULL) {
                        cout << current->data << endl;
                        current = current->next;
                }
        }
        else
                cout << "The list is empty.\n";
}
#endif
