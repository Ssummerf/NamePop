#include <vector>
#include <iostream>
#include <fstream>
#include <cctype>
#include <string>
#include <sstream>
#include "NameSurferEntry.h"
#include "linked_list.h"
#include "NameSurferDataBase.h"
#include <algorithm>



using namespace std;

NameSurferDataBase:: NameSurferDataBase(){
	getNameData("NamesData.txt");
}

NameSurferDataBase:: NameSurferDataBase(string filename){
	getNameData(filename);
}

void NameSurferDataBase:: getNameData(string filename){
	ifstream filereader;
	filereader.open(filename);
	//Bad name case
   	if (!filereader.is_open()){
   		cout << "Could not open file!" << endl;
   	}

   	//Read each line into a string, create a namesurfer entry for each string
   	string lineRead = "temp 0 0 0 0 0 0 0 0 0 0 0 0 0";
   	NameSurferEntry runner(lineRead);

   	while(getline(filereader, lineRead)){
		
		//Sets up name and rank
		runner = NameSurferEntry(lineRead);
		//Error Occurs here
		database.InsertRear(runner);
		}

   	filereader.close();
}

NameSurferEntry NameSurferDataBase::findEntry(string name){

	//Convert it to lowercase, uppercase the first letter, and add a space for parsing.
	std::transform( name.begin(), name.end(), name.begin(), 
                       [] ( char c ) { return ( tolower( c ) ); } );
	name[0] = toupper(name[0]);
	name = name + " ";

	NameSurferEntry temp(name);
	bool checker = database.Remove(temp);
	if(checker)
		database.InsertRear(temp);


	return temp;
}
