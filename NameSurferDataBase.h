#ifndef NAMESURFERDATABASEH
#define NAMESURFERDATABASEH

#include "NameSurferEntry.h"

using namespace std;

class NameSurferDataBase {

    public:
     NameSurferDataBase();
     NameSurferDataBase(string filename);
     void getNameData(string filename);
     NameSurferEntry findEntry(string name);

    private:
    linked_list<NameSurferEntry> database;

};

#endif