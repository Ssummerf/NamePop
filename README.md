# Name Surfer

This program revolves around parsing a text file containing the names of children and their popularity from 1920 to 2010. 

It's a practice in using abstract architecture and overloading operators to solve a problem. After creating our linked list of information, we then are able to give the user simple text prompts in order to retrieve statistics on a name.

## Entry

This overloads our normal boolean operators so we can properly compare our abstract data entry objects. It takes a string of data and splits it to store properly.

## Database

Our database takes the Data txt and converts it into a linked list of Entries. It ensures that the file properly opens, closes, and is formatted correctly.

## Linked List

A custom version of a linked list, including the insertion, removal, and searching methods that we need to navigate through the data.

## Single Node

A generic Node class.

## Name Surfer

Links our classes together, and handles the user input / output

## Ending Remarks

The objective of our code was provided and supported by Jennifer Polack. The code itself was written by Scott Summerford. 